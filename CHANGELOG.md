# 1.0.0 (2023-06-17)


### Features

* initial release containing elementarize playbook ([988adba](https://gitlab.com/rasmus91/elementarize/commit/988adbada8f00e918efc43449961adf39b0e0e4e))
