# elementarize - v1.0.0

This project exists as a basis of some of my own usage of Ansible to set up my private workstation. This has been changed through several iterations and was published in this current iteration at the request of people in the elementary slack group.

I expect this repo will grow a bit in size as I complete different playbooks for different purposes, and perhaps in time, the repo name may not fit well, we shall see. The thing is: as you try and make roles, etc. more generic, the desire to share them amongst all your projects, and the usefulness of doing so increase exponentially

Since ansible roles and playbooks are quite self documenting I shall not spend a lot of time describing in detail what they do. Read the appropriate yaml files for that.

> NOTE: This repo has been setup with [semantic-release](https://github.com/semantic-release/semantic-release) and creates new release tags based on [angular commit message convention](https://www.conventionalcommits.org/en/v1.0.0/)

## Roles

I will not outline every step of every role, merely point out what they do on a surface level, and what they are useful for.

## Playbooks

The playbooks are pretty self explanatory, each entry here will generally contain a single line describing the purpose of the playbook, and an example of how it should be used.

### [elementarize-ubuntu](./elementarize-ubuntu.yml)

This playbooks purpose is to bring the elementary OS desktop to an Ubuntu or Kubuntu installation, removing the existing desktop environment and changing the login manager to pantheon greeter. This includes *removing the gnome-software package and purging snapd* completely from the system.

#### Usage

```bash
ansible-playbook elementarize-ubuntu.yml --ask-become-pass
```

The playbook does several things as root which is why it needs to be run from a user with sudo access, and doing as shown here, the operator will be prompted for the password.

> NOTE: When running this from a user that has been logged in to a different desktop environment, configuration files may be lying around in their home directory, which will make Pantheon look very odd. I have not seen anything wrong for previous Gnome logins, but everything from the Cursor to the wingpanel do not seem correct if the user has ever been logged into the Plasma desktop.